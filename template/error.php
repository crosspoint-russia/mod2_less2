<h2>Errors:</h2>
<ul>
    <?php if ($error instanceof Traversable) : ?>
        <?php foreach ($error as $item): ?>
            <li><?php echo $item->getMessage(); ?></li>
        <?php endforeach; ?>
    <?php else: ?>
        <li><?php echo $error->getMessage(); ?></li>
    <?php endif; ?>
</ul>