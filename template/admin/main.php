<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h3>Добавление новости:</h3>

<?php include __DIR__ . '/saveForm.php'; ?>

<hr>
<h3>редактирование новостей</h3>

<ol>
<?php foreach ($articles as $article): ?>
    <li style="margin-bottom: 20px;">
        <?php echo $article->title; ?>
        <p>

            <!-- загружаем модель Author в переменную, для исключения последующих запросов в БД -->
            
            <?php if (false !== ($currentAuthor = $article->author)) { ?>
                Автор:  <?php echo $currentAuthor->firstname . ' ' . $currentAuthor->lastname; ?>
            <?php } else { ?>
                Редакционная статья
            <?php } ?>
        </p>
        <a href="/admin/edit/?id=<?php echo $article->id; ?>&passphrase=iddqd">редактировать</a>
        <a href="/admin/delete/?id=<?php echo $article->id; ?>&passphrase=iddqd">удалить</a>
    </li>
<?php endforeach; ?>
</ol>

</body>
</html>