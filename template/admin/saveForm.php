<?php $article = $article ?? new \App\Models\Article(); ?>
<form action="/admin/save/?passphrase=iddqd" method="post">
    <p>Заголовок</p>
    <input type="hidden" name="id" value="<?php echo $article->id;  ?>">
    <input type="text" name="title" value="<?php echo $article->title; ?>">
    <p>Текст:</p>
    <textarea name="text" cols="30" rows="10"><?php echo $article->text; ?></textarea>
    <br>

    <select name="author_id">
        <option value="0">пустой author_id</option>

        <?php
        foreach ($authors as $author) : ?>
        <option value="<?php echo $author->id; ?>"

        <?php if (isset($article->author_id) && $author->id === $article->author_id): ?>
                selected="selected"
        <?php endif; ?>
        >
        <?php echo $author->firstname . ' ' . $author->lastname; ?>
        </option>
        <?php endforeach; ?>

    </select>
    <button type="submit">->save()</button>
</form>