<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
<?php foreach ($articles as $article): ?>
    <li>
        <a href="/article/?id=<?php echo $article->id; ?>"><?php echo $article->title; ?></a>
        <p>
            
            <!-- загружаем модель Author в переменную, для исключения последующих запросов в БД -->
            
            <?php if (false !== ($currentAuthor = $article->author)) { ?>
                Автор:  <?php echo $currentAuthor->firstname . ' ' . $currentAuthor->lastname; ?>
            <?php } else { ?>
                Редакционная статья
            <?php } ?>
        </p>
    </li>
<?php endforeach; ?>
</ul>
<hr>
<ul>
    <li><a href="/admin">вход в админ панель без права доступа</a>
    <li><a href="/admin/?passphrase=iddqd">вход в админ панель с правом доступа</a></li>
    </li>
</ul>
</body>
</html>