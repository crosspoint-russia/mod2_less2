<pre>
<?php

/*
 *
 * Протестируйте работу нового метода, для чего заведите в проекте папку tests и в ней размещайте скрипты,
 * которые наглядно докажут работоспособность вашего кода.
 *
 */

require __DIR__ . '/../autoload.php';

$db = new \App\Db;
$prefix = rand();

/*
 * --------------------------
 * Создаем тестовое окружение
 * --------------------------
 */

// проверка запроса без параметров
$sql = 'CREATE TABLE IF NOT EXISTS '.$prefix.'_tests (id SERIAL, name TEXT) ENGINE = InnoDB';

// запрос и результат запроса
var_dump('db->execute: ' . $sql);
assert(true ===  $db->execute($sql,[]));

?>
    <hr>
<?php

/*
 * --------------------------
 * Основные тесты
 * --------------------------
 */

// проверка запроса с параметрами
$sql = 'INSERT INTO '.$prefix.'_tests (id, name) VALUES (NULL, :name)';

// запрос и результат запроса
var_dump('db->execute: ' . $sql);
assert(true === $db->execute($sql,[':name'=>'execute_test']));

// выборка данных
$sql = 'SELECT * FROM '.$prefix.'_tests WHERE name=:name';

var_dump('db->query: ' . $sql);
$result = $db->query($sql,[':name'=>'execute_test']);

if (false === empty($result)) { ?>
    <h3>MAIN TEST PASSED</h3>
<?php } else { ?>
    <h3>MAIN TEST FAILED</h3>
<?php }

var_dump($result);

?>
    <hr>
<?php

/*
 * --------------------------
 * Очищаем тестовое окружение
 * --------------------------
 */
$sql = 'DROP TABLE '.$prefix.'_tests';
// запрос и результат запроса
var_dump('db->execute: ' . $sql);
assert(true ===  $db->execute($sql,[]));

// TODO: assert();


?>
</pre>
