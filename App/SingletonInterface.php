<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 13.01.2017
 * Time: 11:04
 */

namespace App;


interface SingletonInterface
{
    public static function getInstance();
}