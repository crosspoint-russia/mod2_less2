<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 30.01.2017
 * Time: 13:38
 */

namespace App\Exceptions;


class LogableExceptions extends \Exception
{
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $logger = new \App\ErrorLogger();
        $logger->log($this);
    }
}