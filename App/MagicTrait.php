<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 17.01.2017
 * Time: 14:45
 */

namespace App;


trait MagicTrait
{

    protected $data = [];

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        return $this->data[$name];
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

}