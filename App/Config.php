<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 13.01.2017
 * Time: 11:01
 */

namespace App;


class Config
    implements SingletonInterface
{
    use SingletonTrait;

    public $data = [];

    protected function __construct()
    {
        $this->data = include __DIR__ . '/config/mainConfig.php';
    }

}