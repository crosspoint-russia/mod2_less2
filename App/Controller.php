<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 21:39
 */

namespace App;


abstract class Controller
{
    protected $view = null;
    protected $templatePath = __DIR__ . '/../template';

    public function __construct()
    {
        $this->view = new View();
    }

    protected function access():bool 
    {
        return true;
    }

    protected function beforeAction()
    {
    }

    final public function action($name, $param=null)
    {
        $this->beforeAction();
        
        if ($this->access()) {
            $action = 'action' . $name;
            $this->$action($param);
        } else {
            die('Доступ закрыт');
        }
        
        $this->afterAction();
    }
    
    protected function afterAction()
    {
        $config = \App\Config::getInstance();
        if (true === $config->data['debug']) {
            $this->view->request_uri = $_SERVER['REQUEST_URI'];
            $this->view->display($this->templatePath . '/debug.php');
        }
    }

}