<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 24.01.2017
 * Time: 23:07
 */

namespace App\Controllers\Admin;


use App\AController;

class Index extends AController
{
    public function actionDefault()
    {
        $this->view->articles = \App\Models\Article::findAll();
        $this->view->authors = \App\Models\Author::findAll();
        $this->view->display($this->templatePath . '/admin/main.php');
    }

    public function actionTwo()
    {
        echo 'say hello';
    }
}