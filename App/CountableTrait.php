<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 17.01.2017
 * Time: 14:49
 */

namespace App;


trait CountableTrait
{
    public function count()
    {
        return count($this->data);
    }
}