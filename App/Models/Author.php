<?php

namespace App\Models;

use App\Model;

/**
 * @property string $firstname Имя автора
 * @property string $lastname Фамилия автора
 */

class Author
    extends Model
{
    public static $table = 'authors';

    // массив заполняемых полей
    public static $fillable = [
        'firstname',
        'lastname',
    ];
}